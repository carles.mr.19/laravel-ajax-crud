<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contactos extends Model
{
    protected $fillable = ['nombre', 'apellido', 'email', 'fecha_nacimiento'];
}
