<?php

namespace App\Http\Controllers;

use App\Contactos;
use Illuminate\Http\Request;
use Redirect,Response;

class ContactosController extends Controller
{

    public function index()
    {
        $contactos = Contactos::paginate(10);

        return view('contactos.index')->with('contactos', $contactos);
    }

    public function store(Request $request)
    {
        $idContacto = $request->contacto_id;
        $contacto = Contactos::updateOrCreate(['id' => $idContacto],['nombre' => $request->nombre, 'apellido' => $request->apellido, 'fecha_nacimiento' => $request->fecha_nacimiento, 'email' => $request->email]);

        return Response::json($contacto);
    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $post  = Contactos::where($where)->first();

        return Response::json($post);
    }

    public function destroy($id)
    {
        $post = Contactos::where('id',$id)->delete();
dd($id);
        return Response::json($post);
    }
}
