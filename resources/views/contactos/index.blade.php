@extends('layout.default')

@section('content')

        <div class="float-right">
            <a href="javascript:void(0)" class="btn btn-primary" id="añadir-contacto"> Añadir </a>
        </div>
        <h1 style="font-size: 2.2rem">Lista de Contactos</h1>
        <hr>

        @if(session()->get('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div><br>
        @endif

        <table class="table table-bordered bg-light">
            <thead class="bg-dark" style="color: white">
            <tr>
                <th width="60px" style="text-align: center">ID</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Email</th>
                <th>Fecha Nacimiento</th>
                <th width="150px">Accion</th>
            </tr>
            </thead>
            <tbody id="contactos">
            @foreach($contactos as $contacto)
                <tr id="contacto_id_{{$contacto->id}}">
                    <th style="text-align: center">{{$contacto->id}}</th>
                    <td>{{$contacto->nombre}}</td>
                    <td>{{$contacto->apellido}}</td>
                    <td>{{$contacto->email}}</td>
                    <td>{{$contacto->fecha_nacimiento}}</td>

                    <td align="center">
                        <a class="btn btn-primary btn-sm float-left" title="Edit" id="editar-contacto" data-id="{{$contacto->id}}" href="javascript:void(0)"> Editar </a>
                        <a class="btn btn-danger btn-sm float-left" title="Eliminar" id="eliminar-contacto" data-id="{{$contacto->id}}" href="javascript:void(0)"> Eliminar </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <nav>
            <ul class="pagination justify-content-end">
                {{$contactos->links()}}
            </ul>
        </nav>

        <!-- Modal -->
        <div class="modal fade" id="modalAñadirContacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form id="contactoForm" name="contactoForm" class="form-horizontal">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTitle"></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="contacto_id" id="contacto_id">
                            <div class="form-group">
                                <label for="nombre">Nombre:</label>
                                <input type="text" class="form-control" name="nombre" id="nombre" required>
                            </div>

                            <div class="form-group">
                                <label for="apellido">Apellido:</label>
                                <input type="text" class="form-control" name="apellido" id="apellido" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="text" class="form-control" name="email" id="email" required>
                            </div>

                            <div class="form-group">
                                <label for="fecha_nacimiento">Fecha Nacimiento:</label>
                                <input type="date" class="form-control" name="fecha_nacimiento" id="fecha_nacimiento" max="{{date('Y-m-d')}}" required>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="bAñadir"> Añadir </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#añadir-contacto').click(function () {
                $('#bAñadir').val("contacto-añadir");
                $('#contactoForm').trigger("reset");
                $('#modalTitle').html("Añadir Contacto");
                $('#modalAñadirContacto').modal('show');
            });

            $('body').on('click', '#editar-contacto', function () {
                var contacto_id = $(this).data('id');

                $.get('contactos/'+contacto_id +'/edit', function (data) {
                    $('#modalTitle').html("Editar Contacto");
                    $('#bAñadir').val("contacto-editar");
                    $('#modalAñadirContacto').modal('show');
                    $('#contacto_id').val(data.id);
                    $('#nombre').val(data.nombre);
                    $('#apellido').val(data.apellido);
                    $('#email').val(data.email);
                    $('#fecha_nacimiento').val(data.fecha_nacimiento);
                })
            });

            $('body').on('click', '#eliminar-contacto', function () {
                var contacto_id = $(this).data('id');
                confirm("Estas seguro que quieres eliminar el contacto?");

                $.ajax({
                    type: "DELETE",
                    url: "{{ url('contactos')}}"+'/'+contacto_id,
                    success: function (data) {
                        $("#contacto_id_" + contacto_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            });
        });

        if ($("#contactoForm").length > 0) {
            $("#contactoForm").validate({
                submitHandler: function(form) {
                    var actionType = $('#bAñadir').val();
                    $('#bAñadir').html('Enviando...');

                    $.ajax({
                        data: $('#contactoForm').serialize(),
                        url: "{{ route('contactos.store') }}",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            var post = '<tr id="contacto_id_' + data.id + '"><td style="text-align: center">' + data.id + '</td><td>' + data.nombre + '</td><td>' + data.apellido + '</td><td>' + data.email + '</td><td>' + data.fecha_nacimiento + '</td>';
                            post += '<td><a href="javascript:void(0)" id="editar-contacto" data-id="' + data.id + '" class="btn btn-primary btn-sm float-left">Editar</a>';
                            post += '<a href="javascript:void(0)" id="eliminar-contacto" data-id="' + data.id + '" class="btn btn-danger btn-sm float-left eliminar-contacto">Eliminar</a></td></tr>';


                            if (actionType == "contacto-añadir") {
                                $('#contactos').append(post);
                            } else {
                                $("#contacto_id_" + data.id).replaceWith(post);
                            }

                            $('#contactoForm').trigger("reset");
                            $('#modalAñadirContacto').modal('hide');
                            $('#bAñadir').html('Guardar Cambios');

                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }
            })
        }

    </script>

@endsection